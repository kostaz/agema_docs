################################
 Agema NOS Initial Installation
################################

Prerequisites
-------------

Prerequisites should be present in ``/var/www/html`` directory on DHCP server:

- All ``onie-installer-*`` files.
- The bundle file (for example: ``19.1.1.431.bundle``)
- The bundle version files (``AS7316.ver`` and ``Z4806.ver``)

Note
----
The bundle version file contains only line which is
the version of the bundle file). For example::

   $ cat AS7316.ver
   19.1.1.567

How it works?
-------------
The ``onie-installer`` script installs basic Yocto Linux on "rescue" partition
and boots into it. When this "rescue" Linux boots, it installs
the bundle to both partition sets (A and B) and updates the
GRUB menu default to partition set A.

.. raw:: latex

    \newpage

Partitions on Z4806 device
--------------------------

See below::

   ONIE:/ # sgdisk -p /dev/sda
   Disk /dev/sda: 62533296 sectors, 29.8 GiB
   Logical sector size: 512 bytes
   Disk identifier (GUID): C0B93F2A-92D4-4322-94FB-F428A0073641
   Partition table holds up to 128 entries
   First usable sector is 34, last usable sector is 62533262
   Partitions will be aligned on 2048-sector boundaries
   Total free space is 10595954 sectors (5.1 GiB)

   Number  Start (sector)    End (sector)  Size       Code  Name
      1            2048            6143   2.0 MiB     EF02  GRUB-BOOT
      2            6144          268287   128.0 MiB   3000  ONIE-BOOT
      4         4982784         5165057   89.0 MiB    8300  NOS-A-Kernel
      5         5167104        13551621   4.0 GiB     8300  NOS-A-RootFS
      6        13553664        14550186   486.6 MiB   8300  NOS-Shared-Config
      7        14551040        18744494   2.0 GiB     8300  NOS-A-Log
      8        18745344        35517620   8.0 GiB     8300  NOS-Shared-Ver
     10        35518464        35700000   88.6 MiB    8300  NOS-B-Kernel
     11        35700736        39894308   2.0 GiB     8300  NOS-B-Log
     12        39895040        48278829   4.0 GiB     8300  NOS-B-RootFS
     13        48279552        56662639   4.0 GiB     8300  NOS-Rescue

Partitions on AS7316 device
---------------------------

See below::

   A13:~# sgdisk -p /dev/sda
   Disk /dev/sda: 250069680 sectors, 119.2 GiB
   Model: TS128EPTDE1500I
   Sector size (logical/physical): 512/512 bytes
   Disk identifier (GUID): E28243A3-EE56-426C-8299-B5990C469921
   Partition table holds up to 128 entries
   Main partition table begins at sector 2 and ends at sector 33
   First usable sector is 34, last usable sector is 250069646
   Partitions will be aligned on 2048-sector boundaries
   Total free space is 193938034 sectors (92.5 GiB)

   Number  Start (sector)    End (sector)  Size       Code  Name
      1            2048            6143   2.0 MiB     EF02  GRUB-BOOT
      2            6144          268287   128.0 MiB   3000  ONIE-BOOT
      3          268288         4462591   2.0 GiB     8300  ACCTON-DIAG
      4         4982784         5165057   89.0 MiB    8300  NOS-A-Kernel
      5         5167104        13551621   4.0 GiB     8300  NOS-A-RootFS
      6        13553664        14550186   486.6 MiB   8300  NOS-Shared-Config
      7        14551040        18744494   2.0 GiB     8300  NOS-A-Log
      8        18745344        35517620   8.0 GiB     8300  NOS-Shared-Ver
     10        35518464        35700000   88.6 MiB    8300  NOS-B-Kernel
     11        35700736        39894308   2.0 GiB     8300  NOS-B-Log
     12        39895040        48278829   4.0 GiB     8300  NOS-B-RootFS
     13        48279552        56662639   4.0 GiB     8300  NOS-Rescue

.. raw:: latex

    \newpage

