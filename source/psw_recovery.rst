###################
 Password Recovery
###################

This document describes a procedure to recover a password for Agema user.

The password recovery is performed via ONIE Linux.

Specifically, using ``ONIE: Rescue`` GRUB menu entry.

In order to perform the password recovery procedure the host machine
(for example, the Windows laptop) need to be connected to the target device
(Agema White Box router) via serial connection.

Find Out Active Partition
-------------------------

First, let's find out the active partition of NOS (Network Operating System).

There are two NOS partition supported: ``NOS-A`` and ``NOS-B``.

To be more exact, ``NOS-A`` partition is named ``NOS-OPX-A-19.1.1 (716)``
in our real-life example.

And ``NOS-B`` partition is named ``NOS-OPX-B-19.1.1 (723)``.

These are NOS partition names as shown in GRUB menu during the target
device boot. Note, that in order to see GRUB menu the host machine must be
connected to the target device via serial connection.

When GRUB menu is reached during the target device boot, you will immediately
see the default (active) NOS partition - it has an asterisk sign (``*``) on
the left side.

See the example below, where the default (active) NOS partition is ``NOS-A``::

                GNU GRUB  version 2.02

   +------------------------------------------------------------+
   | ONIE: Install OS                                           |
   | ONIE: Rescue                                               |
   | ONIE: Uninstall OS                                         |
   | ONIE: Update ONIE                                          |
   | ONIE: Embed ONIE                                           |
   | Demo DIAG                                                  |
   |*NOS-OPX-A-19.1.1 (716)                                     |
   | NOS-OPX-B-19.1.1 (723)                                     |
   |                                                            |
   |                                                            |
   |                                                            |
   +------------------------------------------------------------+

   Use the ^ and v keys to select which entry is highlighted.
   Press enter to boot the selected OS, `e' to edit the commands
   before booting or `c' for a command-line.

When GRUB menu is shown, press an arrow key (up key or down key) immediately.

Otherwise, device will continue the boot after a short timeout.

Remember (or write down) what is the default (active) partition as this
information is needed during the next steps.

Recovery via ONIE
-----------------

Reboot the target device and wait until the GRUB is reached.

Choose in GRUB menu ``ONIE: Rescue`` entry and press Enter::

                GNU GRUB  version 2.02

   +------------------------------------------------------------+
   | ONIE: Install OS                                           |
   |*ONIE: Rescue                                               |
   | ONIE: Uninstall OS                                         |
   | ONIE: Update ONIE                                          |
   | ONIE: Embed ONIE                                           |
   | Demo DIAG                                                  |
   | NOS-OPX-A-19.1.1 (716)                                     |
   | NOS-OPX-B-19.1.1 (723)                                     |
   |                                                            |
   |                                                            |
   |                                                            |
   +------------------------------------------------------------+

   Use the ^ and v keys to select which entry is highlighted.
   Press enter to boot the selected OS, `e' to edit the commands
   before booting or `c' for a command-line.

Wait for Linux to finish the boot.

It is helpful to note, that rootfs of ``NOS-A`` partition resides on
``/dev/sda5`` disk partition. And rootfs of ``NOS-B`` partition resides
on ``/dev/sda12`` disk partition.

In short:

- Rootfs of ``NOS-A`` is on ``/dev/sda5`` dist partition.
- Rootfs of ``NOS-B`` is on ``/dev/sda12`` dist partition.

To perform password recovery for ``NOS-A`` partition - perform the next steps::

   $ cd /tmp
   $ mkdir sda5
   $ mount /dev/sda5 sda5
   $ cd sda5

   $ cp etc/shadow etc/shadow_`date +%Y-%m-%d.%H:%M:%S`
   $ ls -ls etc/shadow*

   $ sed -i -E 's/root:([^:]+)(.+)/root:\2/g' etc/shadow
   $ diff etc/shadow_* etc/shadow

   $ sync
   $ reboot

Wait for the device to perform the reboot and reach GRUB menu again.

Choose in GRUB menu ``NOS-A`` partition (in our example this is
``NOS-OPX-A-19.1.1 (716)``) and press enter.

Wait for the system to reach a login prompt::

   INFO Starting AMF...
   INFO Starting AMF watchdog...
   backup previous logs...              done
   Startup configuration loaded successfully

   ADVA Optical Networking FSP 150-AS7316 19.1.1 (716)
   A14 login:

Now login as root **WITHOUT** password (just press Enter with empty password)::

   ADVA Optical Networking FSP 150-AS7316 19.1.1 (716)
   A14 login: root
   Password:
   Last login: Mon Jan 21 14:00:33 UTC 2019 from A14 on ttyS0
   A14:~#

And the last step is to define a password for both ``root`` and ``admin`` users::

   A14:~# passwd root
   New password:
   Retype new password:
   passwd: password updated successfully
   A14:~#

   A14:~# passwd admin
   New password:
   Retype new password:
   passwd: password updated successfully
   A14:~#

The password may be tested by logging out and then logging in.

**All is done successfully!**

To perform password recovery for ``NOS-B`` partition - do the same steps as for
``NOS-A`` except use ``sda12`` instead of ``sda5``.

See ready for copy-paste commands below::

   $ cd /tmp
   $ mkdir sda12
   $ mount /dev/sda12 sda12
   $ cd sda12

   $ cp etc/shadow etc/shadow_`date +%Y-%m-%d.%H:%M:%S`
   $ ls -ls etc/shadow*

   $ sed -i -E 's/root:([^:]+)(.+)/root:\2/g' etc/shadow
   $ diff etc/shadow_* etc/shadow

   $ sync
   $ reboot


