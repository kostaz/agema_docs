###########
 SW Update
###########

Agema product is based on x86 CPU and contains SSD disk like a regular
personal computer or laptop. Agema's software is installed on this SSD disk.
Also, the device configuration is stored on the SSD disk.

Agema is a White Box router solution, based on ONIE.
ONIE is `Open Network Install Environment`_ - an Open Compute Project open source
initiative.

ONIE is a great Linux project and its log is:

.. image:: images/onie_logo.png

.. raw:: latex

    \newpage

ONIE's Highlights:

- Combines a boot loader with a modern Linux kernel and BusyBox
- Provides an environment for installing any network OS
- Helps automate large scale data center switch provisioning
- Manage your switches like you manage your Linux servers
- Disruptive, liberating users from a captive, pre-installed network OS

.. raw:: latex

    \newpage

Effectively, when R&D receives the white box device it already has
ONIE pre-installed. It means that SSD disk of the device has some disk
partitions, installed boot loader (GRUB in our case), Linux kernel and
file system.

Specifically, the below partitions exist:

- GRUB-BOOT
- ONIE-BOOT
- DIAG

ADVA Network Operating System (NOS) is installed alongside ONIE system.
New partitions are created during NOS initial installation to contain
Agema NOS. Note, that the partitions are created only during the initial
installation of Agema NOS. During software upgrade disk partitions are
just used (mounted, read and written).

Agema NOS disk partitions:

- NOS Partition A

  - NOS-A-Kernel
  - NOS-A-RootFS
  - NOS-A-Log

- NOS Partition B

  - NOS-B-Kernel
  - NOS-B-RootFS
  - NOS-B-Log

- Shared Partitions

  - NOS-Shared-Config
  - NOS-Shared-Ver
  - NOS-Rescue

During Agema NOS software update a bundle file is downloaded to
the device disk first. The bundle (for example: ``19.1.1.431.bundle``)
contains all the software components of the new release:

- Linux kernel
- File system

The bundle is an executable file. When it is executed, it unpacks
itself to the disk and copies Linux kernel and file system to the
free partition set (for example, to ``NOS-B-Kernel`` and ``NOS-B-RootFS``).

GRUB default boot option is updated and the device is rebooted.

The next boot, the new default GRUB boot menu entry is activated,
that boots the newly installed Agema NOS release.

.. raw:: latex

    \newpage

.. _Open Network Install Environment: http://www.onie.org/about/
